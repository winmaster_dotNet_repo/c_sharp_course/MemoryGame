﻿using KatarzynaBialasLab4MemoryGame.Controler.Command.Interfaces;
using KatarzynaBialasLab4MemoryGame.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaBialasLab4MemoryGame.Controler.Command
{
    public class WriteRepository<T> : IWriteRepository<T> where T : Entity
    {
        protected readonly KatarzynaBialas _context;

        public WriteRepository(KatarzynaBialas context)
        {
            _context = context;
        }

        public void Create(T entity)
        {
            _context.Set<T>().Add(entity);
            _context.SaveChanges();
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }

        public void Edit(T entity)
        {
            //_context.Set<T>().
        }
    }
}
