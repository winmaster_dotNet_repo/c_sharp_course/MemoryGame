﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaBialasLab4MemoryGame.Controler.Command.Interfaces
{
    public interface IWriteRepository<T>
    {
        void Create(T entity);
        void Edit(T entity);
        void Delete(T entity);
    }
}
