﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaBialasLab4MemoryGame.Controler.Query.Interfaces
{
    public interface IReadRepository<T>
    {
        IList<T> GetAll();
        T GetById(int id);
    }
}
