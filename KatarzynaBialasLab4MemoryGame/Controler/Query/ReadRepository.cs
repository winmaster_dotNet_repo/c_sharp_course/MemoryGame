﻿using KatarzynaBialasLab4MemoryGame.Controler.Query.Interfaces;
using KatarzynaBialasLab4MemoryGame.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaBialasLab4MemoryGame.Controler.Query
{
    public class ReadRepository<T> : IReadRepository<T> where T : Entity
    {
        protected readonly KatarzynaBialas _context;

        public ReadRepository(KatarzynaBialas context)
        {
            _context = context;
            _context.SaveChanges();
        }

        public IList<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }
        public T GetById(int id)
        {
            return _context.Set<T>().Where(x => x.Id == id).FirstOrDefault();
        }
    }
}
