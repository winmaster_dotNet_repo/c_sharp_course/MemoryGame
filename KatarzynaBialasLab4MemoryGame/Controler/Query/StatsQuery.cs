﻿using KatarzynaBialasLab4MemoryGame.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaBialasLab4MemoryGame.Controler.Query
{
    public class StatsQuery : ReadRepository<Result>
    {
        public StatsQuery(KatarzynaBialas context) : base (context)
        {

        }

        public List<Result> GetGamersResults()
        {
            return _context.Set<Result>().ToList();
        }
    }
}
