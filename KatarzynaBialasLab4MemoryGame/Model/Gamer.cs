﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaBialasLab4MemoryGame.Model
{
    public class Gamer : Entity
    {
        public Gamer()
        {
            Results = new List<Result>();
        }

        public string Nick { get; set; }
        public string Password { get; set; }
        public IList<Result> Results { get; set; }
    }
}
