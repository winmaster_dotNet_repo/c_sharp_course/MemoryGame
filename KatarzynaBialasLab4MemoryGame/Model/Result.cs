﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatarzynaBialasLab4MemoryGame.Model
{
    public class Result : Entity
    {
        public string Type { get; set; }
        public int Time { get; set; }
        public string GamerNick { get; set; }
    }
}
