﻿namespace KatarzynaBialasLab4MemoryGame.Model
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class KatarzynaBialas : DbContext
    {

        public KatarzynaBialas()
            : base("name=KatarzynaBialas")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<KatarzynaBialas>());
        }

        public virtual DbSet<Gamer> Gamers { get; set; }
        public virtual DbSet<Result> Results { get; set; }
    }
}

