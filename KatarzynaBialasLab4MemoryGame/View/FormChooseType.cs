﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab4MemoryGame
{
    public partial class FormChooseType : Form
    {
        public FormChooseType()
        {
            InitializeComponent();
        }

        private void buttonSmall_Click(object sender, EventArgs e)
        {
            FormGameSmall formGameSmall = new FormGameSmall();
            formGameSmall.ShowDialog();
        }

        private void buttonMedium_Click(object sender, EventArgs e)
        {
            FormGameMedium formGameMedium = new FormGameMedium();
            formGameMedium.ShowDialog();
        }

        private void buttonBig_Click(object sender, EventArgs e)
        {
            FormGameBig formGameBig = new FormGameBig();
            formGameBig.ShowDialog();
        }
    }
}
