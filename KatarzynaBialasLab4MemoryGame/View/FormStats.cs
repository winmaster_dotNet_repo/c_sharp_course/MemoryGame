﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab4MemoryGame
{
    public partial class FormStats : Form
    {
        private SqlConnection sqlConnection;
        private SqlDataAdapter sqlDataAdapter;

        public FormStats()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source = localhost; database = KatarzynaBialasLab4MemoryGame.KatarzynaBialas; Trusted_Connection= yes");
            StatToDataGridView.GetAllGameResults(sqlConnection, sqlDataAdapter, dataGridViewStats);
        }

        private void button4x4_Click(object sender, EventArgs e)
        {
            StatToDataGridView.GetAllResultsWithSpecifiedType(sqlConnection, sqlDataAdapter, dataGridViewStats, "4x4");
        }

        private void button4x5_Click(object sender, EventArgs e)
        {
            StatToDataGridView.GetAllResultsWithSpecifiedType(sqlConnection, sqlDataAdapter, dataGridViewStats, "4x5");
        }

        private void button6x6_Click(object sender, EventArgs e)
        {
            StatToDataGridView.GetAllResultsWithSpecifiedType(sqlConnection, sqlDataAdapter, dataGridViewStats, "6x6");
        }
    }
}
