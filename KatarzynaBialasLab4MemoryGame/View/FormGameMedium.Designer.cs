﻿namespace KatarzynaBialasLab4MemoryGame
{
    partial class FormGameMedium
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanelGame = new System.Windows.Forms.TableLayoutPanel();
            this.labelInitialize20 = new System.Windows.Forms.Label();
            this.labelInitialize19 = new System.Windows.Forms.Label();
            this.labelInitialize18 = new System.Windows.Forms.Label();
            this.labelInitialize17 = new System.Windows.Forms.Label();
            this.labelInitialize16 = new System.Windows.Forms.Label();
            this.labelInitialize15 = new System.Windows.Forms.Label();
            this.labelInitialize14 = new System.Windows.Forms.Label();
            this.labelInitialize13 = new System.Windows.Forms.Label();
            this.labelInitialize12 = new System.Windows.Forms.Label();
            this.labelInitialize11 = new System.Windows.Forms.Label();
            this.labelInitialize10 = new System.Windows.Forms.Label();
            this.labelInitialize9 = new System.Windows.Forms.Label();
            this.labelInitialize8 = new System.Windows.Forms.Label();
            this.labelInitialize7 = new System.Windows.Forms.Label();
            this.labelInitialize6 = new System.Windows.Forms.Label();
            this.labelInitialize5 = new System.Windows.Forms.Label();
            this.labelInitialize4 = new System.Windows.Forms.Label();
            this.labelInitialize3 = new System.Windows.Forms.Label();
            this.labelInitialize2 = new System.Windows.Forms.Label();
            this.labelInitialize1 = new System.Windows.Forms.Label();
            this.timerMemory = new System.Windows.Forms.Timer(this.components);
            this.timerResultTime = new System.Windows.Forms.Timer(this.components);
            this.timerAction = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanelGame.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelGame
            // 
            this.tableLayoutPanelGame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.tableLayoutPanelGame.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanelGame.ColumnCount = 4;
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize20, 3, 4);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize19, 2, 4);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize18, 1, 4);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize17, 0, 4);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize16, 3, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize15, 2, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize14, 1, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize13, 0, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize12, 3, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize11, 2, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize10, 1, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize9, 0, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize8, 3, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize7, 2, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize6, 1, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize5, 0, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize4, 3, 0);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize3, 2, 0);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize2, 1, 0);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize1, 0, 0);
            this.tableLayoutPanelGame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGame.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelGame.Name = "tableLayoutPanelGame";
            this.tableLayoutPanelGame.RowCount = 5;
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanelGame.Size = new System.Drawing.Size(762, 565);
            this.tableLayoutPanelGame.TabIndex = 0;
            // 
            // labelInitialize20
            // 
            this.labelInitialize20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize20.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize20.Location = new System.Drawing.Point(575, 450);
            this.labelInitialize20.Name = "labelInitialize20";
            this.labelInitialize20.Size = new System.Drawing.Size(182, 113);
            this.labelInitialize20.TabIndex = 23;
            this.labelInitialize20.Text = "L";
            this.labelInitialize20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize20.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize19
            // 
            this.labelInitialize19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize19.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize19.Location = new System.Drawing.Point(385, 450);
            this.labelInitialize19.Name = "labelInitialize19";
            this.labelInitialize19.Size = new System.Drawing.Size(182, 113);
            this.labelInitialize19.TabIndex = 22;
            this.labelInitialize19.Text = "L";
            this.labelInitialize19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize19.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize18
            // 
            this.labelInitialize18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize18.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize18.Location = new System.Drawing.Point(195, 450);
            this.labelInitialize18.Name = "labelInitialize18";
            this.labelInitialize18.Size = new System.Drawing.Size(182, 113);
            this.labelInitialize18.TabIndex = 21;
            this.labelInitialize18.Text = "L";
            this.labelInitialize18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize18.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize17
            // 
            this.labelInitialize17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize17.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize17.Location = new System.Drawing.Point(5, 450);
            this.labelInitialize17.Name = "labelInitialize17";
            this.labelInitialize17.Size = new System.Drawing.Size(182, 113);
            this.labelInitialize17.TabIndex = 20;
            this.labelInitialize17.Text = "L";
            this.labelInitialize17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize17.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize16
            // 
            this.labelInitialize16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize16.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize16.Location = new System.Drawing.Point(575, 338);
            this.labelInitialize16.Name = "labelInitialize16";
            this.labelInitialize16.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize16.TabIndex = 18;
            this.labelInitialize16.Text = "L";
            this.labelInitialize16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize16.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize15
            // 
            this.labelInitialize15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize15.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize15.Location = new System.Drawing.Point(385, 338);
            this.labelInitialize15.Name = "labelInitialize15";
            this.labelInitialize15.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize15.TabIndex = 17;
            this.labelInitialize15.Text = "L";
            this.labelInitialize15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize15.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize14
            // 
            this.labelInitialize14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize14.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize14.Location = new System.Drawing.Point(195, 338);
            this.labelInitialize14.Name = "labelInitialize14";
            this.labelInitialize14.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize14.TabIndex = 16;
            this.labelInitialize14.Text = "L";
            this.labelInitialize14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize14.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize13
            // 
            this.labelInitialize13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize13.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize13.Location = new System.Drawing.Point(5, 338);
            this.labelInitialize13.Name = "labelInitialize13";
            this.labelInitialize13.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize13.TabIndex = 15;
            this.labelInitialize13.Text = "L";
            this.labelInitialize13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize13.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize12
            // 
            this.labelInitialize12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize12.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize12.Location = new System.Drawing.Point(575, 226);
            this.labelInitialize12.Name = "labelInitialize12";
            this.labelInitialize12.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize12.TabIndex = 13;
            this.labelInitialize12.Text = "L";
            this.labelInitialize12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize12.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize11
            // 
            this.labelInitialize11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize11.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize11.Location = new System.Drawing.Point(385, 226);
            this.labelInitialize11.Name = "labelInitialize11";
            this.labelInitialize11.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize11.TabIndex = 12;
            this.labelInitialize11.Text = "L";
            this.labelInitialize11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize11.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize10
            // 
            this.labelInitialize10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize10.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize10.Location = new System.Drawing.Point(195, 226);
            this.labelInitialize10.Name = "labelInitialize10";
            this.labelInitialize10.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize10.TabIndex = 11;
            this.labelInitialize10.Text = "L";
            this.labelInitialize10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize10.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize9
            // 
            this.labelInitialize9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize9.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize9.Location = new System.Drawing.Point(5, 226);
            this.labelInitialize9.Name = "labelInitialize9";
            this.labelInitialize9.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize9.TabIndex = 10;
            this.labelInitialize9.Text = "L";
            this.labelInitialize9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize9.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize8
            // 
            this.labelInitialize8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize8.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize8.Location = new System.Drawing.Point(575, 114);
            this.labelInitialize8.Name = "labelInitialize8";
            this.labelInitialize8.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize8.TabIndex = 8;
            this.labelInitialize8.Text = "L";
            this.labelInitialize8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize8.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize7
            // 
            this.labelInitialize7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize7.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize7.Location = new System.Drawing.Point(385, 114);
            this.labelInitialize7.Name = "labelInitialize7";
            this.labelInitialize7.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize7.TabIndex = 7;
            this.labelInitialize7.Text = "L";
            this.labelInitialize7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize7.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize6
            // 
            this.labelInitialize6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize6.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize6.Location = new System.Drawing.Point(195, 114);
            this.labelInitialize6.Name = "labelInitialize6";
            this.labelInitialize6.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize6.TabIndex = 6;
            this.labelInitialize6.Text = "L";
            this.labelInitialize6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize6.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize5
            // 
            this.labelInitialize5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize5.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize5.Location = new System.Drawing.Point(5, 114);
            this.labelInitialize5.Name = "labelInitialize5";
            this.labelInitialize5.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize5.TabIndex = 5;
            this.labelInitialize5.Text = "L";
            this.labelInitialize5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize5.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize4
            // 
            this.labelInitialize4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize4.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize4.Location = new System.Drawing.Point(575, 2);
            this.labelInitialize4.Name = "labelInitialize4";
            this.labelInitialize4.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize4.TabIndex = 3;
            this.labelInitialize4.Text = "L";
            this.labelInitialize4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize4.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize3
            // 
            this.labelInitialize3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize3.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize3.Location = new System.Drawing.Point(385, 2);
            this.labelInitialize3.Name = "labelInitialize3";
            this.labelInitialize3.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize3.TabIndex = 2;
            this.labelInitialize3.Text = "L";
            this.labelInitialize3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize3.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize2
            // 
            this.labelInitialize2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize2.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize2.Location = new System.Drawing.Point(195, 2);
            this.labelInitialize2.Name = "labelInitialize2";
            this.labelInitialize2.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize2.TabIndex = 1;
            this.labelInitialize2.Text = "L";
            this.labelInitialize2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize2.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize1
            // 
            this.labelInitialize1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize1.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize1.Location = new System.Drawing.Point(5, 2);
            this.labelInitialize1.Name = "labelInitialize1";
            this.labelInitialize1.Size = new System.Drawing.Size(182, 110);
            this.labelInitialize1.TabIndex = 0;
            this.labelInitialize1.Text = "L";
            this.labelInitialize1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize1.Click += new System.EventHandler(this.memoClick);
            // 
            // timerMemory
            // 
            this.timerMemory.Interval = 800;
            this.timerMemory.Tick += new System.EventHandler(this.timerMemory_Tick_1);
            // 
            // timerResultTime
            // 
            this.timerResultTime.Interval = 1;
            this.timerResultTime.Tick += new System.EventHandler(this.timerResultTime_Tick_1);
            // 
            // timerAction
            // 
            this.timerAction.Interval = 1;
            this.timerAction.Tick += new System.EventHandler(this.timerAction_Tick_1);
            // 
            // FormGameMedium
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(762, 565);
            this.Controls.Add(this.tableLayoutPanelGame);
            this.Name = "FormGameMedium";
            this.Text = "Gra Memory";
            this.tableLayoutPanelGame.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGame;
        private System.Windows.Forms.Label labelInitialize1;
        private System.Windows.Forms.Label labelInitialize20;
        private System.Windows.Forms.Label labelInitialize19;
        private System.Windows.Forms.Label labelInitialize18;
        private System.Windows.Forms.Label labelInitialize17;
        private System.Windows.Forms.Label labelInitialize16;
        private System.Windows.Forms.Label labelInitialize15;
        private System.Windows.Forms.Label labelInitialize14;
        private System.Windows.Forms.Label labelInitialize13;
        private System.Windows.Forms.Label labelInitialize12;
        private System.Windows.Forms.Label labelInitialize11;
        private System.Windows.Forms.Label labelInitialize10;
        private System.Windows.Forms.Label labelInitialize9;
        private System.Windows.Forms.Label labelInitialize8;
        private System.Windows.Forms.Label labelInitialize7;
        private System.Windows.Forms.Label labelInitialize6;
        private System.Windows.Forms.Label labelInitialize5;
        private System.Windows.Forms.Label labelInitialize4;
        private System.Windows.Forms.Label labelInitialize3;
        private System.Windows.Forms.Label labelInitialize2;
        private System.Windows.Forms.Timer timerMemory;
        private System.Windows.Forms.Timer timerResultTime;
        private System.Windows.Forms.Timer timerAction;
    }
}