﻿namespace KatarzynaBialasLab4MemoryGame
{
    partial class FormStats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxBrain = new System.Windows.Forms.PictureBox();
            this.dataGridViewStats = new System.Windows.Forms.DataGridView();
            this.labelScores = new System.Windows.Forms.Label();
            this.button4x4 = new System.Windows.Forms.Button();
            this.button4x5 = new System.Windows.Forms.Button();
            this.button6x6 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBrain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStats)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxBrain
            // 
            this.pictureBoxBrain.Image = global::KatarzynaBialasLab4MemoryGame.Properties.Resources.memory3;
            this.pictureBoxBrain.Location = new System.Drawing.Point(21, 309);
            this.pictureBoxBrain.Name = "pictureBoxBrain";
            this.pictureBoxBrain.Size = new System.Drawing.Size(141, 132);
            this.pictureBoxBrain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBrain.TabIndex = 0;
            this.pictureBoxBrain.TabStop = false;
            // 
            // dataGridViewStats
            // 
            this.dataGridViewStats.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStats.Location = new System.Drawing.Point(12, 48);
            this.dataGridViewStats.Name = "dataGridViewStats";
            this.dataGridViewStats.Size = new System.Drawing.Size(313, 255);
            this.dataGridViewStats.TabIndex = 1;
            // 
            // labelScores
            // 
            this.labelScores.AutoSize = true;
            this.labelScores.Font = new System.Drawing.Font("Forte", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScores.Location = new System.Drawing.Point(68, 9);
            this.labelScores.Name = "labelScores";
            this.labelScores.Size = new System.Drawing.Size(193, 26);
            this.labelScores.TabIndex = 2;
            this.labelScores.Text = "Najlepsze wyniki:";
            // 
            // button4x4
            // 
            this.button4x4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button4x4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button4x4.Location = new System.Drawing.Point(207, 319);
            this.button4x4.Name = "button4x4";
            this.button4x4.Size = new System.Drawing.Size(75, 35);
            this.button4x4.TabIndex = 3;
            this.button4x4.Text = "4x4";
            this.button4x4.UseVisualStyleBackColor = false;
            this.button4x4.Click += new System.EventHandler(this.button4x4_Click);
            // 
            // button4x5
            // 
            this.button4x5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button4x5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button4x5.Location = new System.Drawing.Point(207, 360);
            this.button4x5.Name = "button4x5";
            this.button4x5.Size = new System.Drawing.Size(75, 35);
            this.button4x5.TabIndex = 4;
            this.button4x5.Text = "4x5";
            this.button4x5.UseVisualStyleBackColor = false;
            this.button4x5.Click += new System.EventHandler(this.button4x5_Click);
            // 
            // button6x6
            // 
            this.button6x6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button6x6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button6x6.Location = new System.Drawing.Point(207, 401);
            this.button6x6.Name = "button6x6";
            this.button6x6.Size = new System.Drawing.Size(75, 35);
            this.button6x6.TabIndex = 5;
            this.button6x6.Text = "6x6";
            this.button6x6.UseVisualStyleBackColor = false;
            this.button6x6.Click += new System.EventHandler(this.button6x6_Click);
            // 
            // FormStats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(344, 453);
            this.Controls.Add(this.button6x6);
            this.Controls.Add(this.button4x5);
            this.Controls.Add(this.button4x4);
            this.Controls.Add(this.labelScores);
            this.Controls.Add(this.dataGridViewStats);
            this.Controls.Add(this.pictureBoxBrain);
            this.Name = "FormStats";
            this.Text = "Statystyki";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBrain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStats)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxBrain;
        private System.Windows.Forms.DataGridView dataGridViewStats;
        private System.Windows.Forms.Label labelScores;
        private System.Windows.Forms.Button button4x4;
        private System.Windows.Forms.Button button4x5;
        private System.Windows.Forms.Button button6x6;
    }
}