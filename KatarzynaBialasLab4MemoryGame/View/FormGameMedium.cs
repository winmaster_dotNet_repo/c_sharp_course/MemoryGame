﻿using KatarzynaBialasLab4MemoryGame.Controler.Command;
using KatarzynaBialasLab4MemoryGame.Controler.Command.Interfaces;
using KatarzynaBialasLab4MemoryGame.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab4MemoryGame
{
    public partial class FormGameMedium : Form
    {
        Random random = new Random();
        Label labelFirstClicked = null;
        Label labelSecondClicked = null;
        int gameResult;
        int waitingTime;
        bool startGame = false;
        SoundPlayer simpleSound;
        string playerNick = FormMember.nick;
        private IWriteRepository<Result> _resultCommand;
        private readonly KatarzynaBialas _context;

        List<string> gameIcons = new List<string>()
        {

        "h", "h", "j", "j", "I", "I", "$", "$", "P", "P",
        "Q", "Q", "W", "W", "S", "S", "B", "B", "D", "D" 
        };

        private void AssignIconsToSquares()
        {

            foreach (Control control in tableLayoutPanelGame.Controls)
            {
                Label iconLabel = control as Label;
                if (iconLabel != null)
                {
                    int randomNumber = random.Next(gameIcons.Count);
                    iconLabel.Text = gameIcons[randomNumber];
                    iconLabel.ForeColor = iconLabel.BackColor;
                    gameIcons.RemoveAt(randomNumber);
                }
            }
        }

        private void playSimpleSound()
        {
            Assembly assembly;
            assembly = Assembly.GetExecutingAssembly();
            simpleSound = new SoundPlayer(assembly.GetManifestResourceStream("KatarzynaBialasLab4MemoryGame.soundToGame1.wav"));
            simpleSound.PlayLooping();
        }

        private void stopPlayingSimpleSound()
        {
            simpleSound.Stop();
        }

        private void SetInitializeIcons()
        {
            foreach (Control control in tableLayoutPanelGame.Controls)
            {
                Label iconLabel = control as Label;
                iconLabel.Text = "L";
            }
        }

        public FormGameMedium()
        {
            _context = new KatarzynaBialas();
            _resultCommand = new WriteRepository<Result>(_context);
            InitializeComponent();
            playSimpleSound();
            this.Text = playerNick + " Twój czas: " + gameResult;
            timerAction.Start();
            SetInitializeIcons();
        }

          private void CheckForWinner()
        {
            foreach (Control control in tableLayoutPanelGame.Controls)
            {
                Label iconLabel = control as Label;

                if (iconLabel != null)
                {
                    if (iconLabel.ForeColor == iconLabel.BackColor)
                        return;
                }
            }
            timerResultTime.Stop();
            Result result = new Result() //szybka inicjalizacja obiektu
            {
                Type = "4x5",
                Time = gameResult,
                GamerNick = playerNick
            };
            _resultCommand.Create(result);
            MessageBox.Show("Wygrałeś!  Twój czas to: " + gameResult + " ms", "Gratulacje! " + playerNick);
            stopPlayingSimpleSound();
            Close();
        }

       private void timerAction_Tick_1(object sender, EventArgs e)
        {
            if (waitingTime != 150)
                waitingTime++;
            else
            {
                timerAction.Stop();
                startGame = true;
                AssignIconsToSquares();
                timerResultTime.Start();
            }
        }

        private void timerResultTime_Tick_1(object sender, EventArgs e)
        {
            gameResult++;
            this.Text = playerNick + " Twój czas: " + gameResult;
        }

        private void timerMemory_Tick_1(object sender, EventArgs e)
        {
            timerMemory.Stop();
            // ukryj obie karty memory
            labelFirstClicked.ForeColor = labelFirstClicked.BackColor;
            labelSecondClicked.ForeColor = labelSecondClicked.BackColor;
            //wyzeruj pierwsze i drugie klikniecie
            labelFirstClicked = null;
            labelSecondClicked = null;
        }

        private void memoClick(object sender, EventArgs e)
        {
            if (startGame)
            {
                if (timerMemory.Enabled == true)
                    return;

                Label clickedLabel = sender as Label;

                if (clickedLabel != null)
                {
                    if (clickedLabel.ForeColor == Color.Black)
                        return;
                    if (labelFirstClicked == null)
                    {
                        labelFirstClicked = clickedLabel;
                        labelFirstClicked.ForeColor = Color.Black;
                        return;
                    }
                    labelSecondClicked = clickedLabel;
                    labelSecondClicked.ForeColor = Color.Black;

                    CheckForWinner();

                    if (labelFirstClicked.Text == labelSecondClicked.Text)
                    {
                        labelFirstClicked = null;
                        labelSecondClicked = null;
                        return;
                    }
                    timerMemory.Start();
                }
            }
        }
    }

}
