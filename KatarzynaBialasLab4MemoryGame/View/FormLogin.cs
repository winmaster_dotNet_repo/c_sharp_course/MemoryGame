﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab4MemoryGame
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void buttonLoginMember_Click(object sender, EventArgs e)
        {
            FormMember formMember = new FormMember();
            formMember.ShowDialog();
        }

        private void buttonLoginNewMember_Click(object sender, EventArgs e)
        {
            FormNewMember formNewMember = new FormNewMember();
            formNewMember.ShowDialog();
        }

        private void buttonStats_Click(object sender, EventArgs e)
        {
             FormStats formStats = new FormStats();
             formStats.ShowDialog();
        }
    }
}
