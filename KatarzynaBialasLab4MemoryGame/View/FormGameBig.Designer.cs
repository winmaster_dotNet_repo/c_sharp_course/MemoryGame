﻿namespace KatarzynaBialasLab4MemoryGame
{
    partial class FormGameBig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanelGame = new System.Windows.Forms.TableLayoutPanel();
            this.labelInitialize36 = new System.Windows.Forms.Label();
            this.labelInitialize35 = new System.Windows.Forms.Label();
            this.labelInitialize34 = new System.Windows.Forms.Label();
            this.labelInitialize33 = new System.Windows.Forms.Label();
            this.labelInitialize32 = new System.Windows.Forms.Label();
            this.labelInitialize31 = new System.Windows.Forms.Label();
            this.labelInitialize30 = new System.Windows.Forms.Label();
            this.labelInitialize29 = new System.Windows.Forms.Label();
            this.labelInitialize28 = new System.Windows.Forms.Label();
            this.labelInitialize27 = new System.Windows.Forms.Label();
            this.labelInitialize26 = new System.Windows.Forms.Label();
            this.labelInitialize25 = new System.Windows.Forms.Label();
            this.labelInitialize24 = new System.Windows.Forms.Label();
            this.labelInitialize23 = new System.Windows.Forms.Label();
            this.labelInitialize22 = new System.Windows.Forms.Label();
            this.labelInitialize21 = new System.Windows.Forms.Label();
            this.labelInitialize20 = new System.Windows.Forms.Label();
            this.labelInitialize19 = new System.Windows.Forms.Label();
            this.labelInitialize18 = new System.Windows.Forms.Label();
            this.labelInitialize17 = new System.Windows.Forms.Label();
            this.labelInitialize16 = new System.Windows.Forms.Label();
            this.labelInitialize15 = new System.Windows.Forms.Label();
            this.labelInitialize14 = new System.Windows.Forms.Label();
            this.labelInitialize13 = new System.Windows.Forms.Label();
            this.labelInitialize12 = new System.Windows.Forms.Label();
            this.labelInitialize11 = new System.Windows.Forms.Label();
            this.labelInitialize10 = new System.Windows.Forms.Label();
            this.labelInitialize9 = new System.Windows.Forms.Label();
            this.labelInitialize8 = new System.Windows.Forms.Label();
            this.labelInitialize7 = new System.Windows.Forms.Label();
            this.labelInitialize6 = new System.Windows.Forms.Label();
            this.labelInitialize5 = new System.Windows.Forms.Label();
            this.labelInitialize4 = new System.Windows.Forms.Label();
            this.labelInitialize3 = new System.Windows.Forms.Label();
            this.labelInitialize2 = new System.Windows.Forms.Label();
            this.labelInitialize1 = new System.Windows.Forms.Label();
            this.timerMemory = new System.Windows.Forms.Timer(this.components);
            this.timerResultTime = new System.Windows.Forms.Timer(this.components);
            this.timerAction = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanelGame.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelGame
            // 
            this.tableLayoutPanelGame.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.tableLayoutPanelGame.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanelGame.ColumnCount = 6;
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize36, 5, 5);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize35, 4, 5);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize34, 3, 5);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize33, 2, 5);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize32, 1, 5);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize31, 0, 5);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize30, 5, 4);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize29, 4, 4);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize28, 3, 4);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize27, 2, 4);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize26, 1, 4);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize25, 0, 4);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize24, 5, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize23, 4, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize22, 3, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize21, 2, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize20, 1, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize19, 0, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize18, 5, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize17, 4, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize16, 3, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize15, 2, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize14, 1, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize13, 0, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize12, 5, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize11, 4, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize10, 3, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize9, 2, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize8, 1, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize7, 0, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize6, 5, 0);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize5, 4, 0);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize4, 3, 0);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize3, 2, 0);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize2, 1, 0);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize1, 0, 0);
            this.tableLayoutPanelGame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGame.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelGame.Name = "tableLayoutPanelGame";
            this.tableLayoutPanelGame.RowCount = 6;
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanelGame.Size = new System.Drawing.Size(862, 733);
            this.tableLayoutPanelGame.TabIndex = 0;
            // 
            // labelInitialize36
            // 
            this.labelInitialize36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize36.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize36.Location = new System.Drawing.Point(720, 607);
            this.labelInitialize36.Name = "labelInitialize36";
            this.labelInitialize36.Size = new System.Drawing.Size(137, 124);
            this.labelInitialize36.TabIndex = 35;
            this.labelInitialize36.Text = "c";
            this.labelInitialize36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize36.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize35
            // 
            this.labelInitialize35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize35.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize35.Location = new System.Drawing.Point(577, 607);
            this.labelInitialize35.Name = "labelInitialize35";
            this.labelInitialize35.Size = new System.Drawing.Size(135, 124);
            this.labelInitialize35.TabIndex = 34;
            this.labelInitialize35.Text = "c";
            this.labelInitialize35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize35.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize34
            // 
            this.labelInitialize34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize34.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize34.Location = new System.Drawing.Point(434, 607);
            this.labelInitialize34.Name = "labelInitialize34";
            this.labelInitialize34.Size = new System.Drawing.Size(135, 124);
            this.labelInitialize34.TabIndex = 33;
            this.labelInitialize34.Text = "c";
            this.labelInitialize34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize34.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize33
            // 
            this.labelInitialize33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize33.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize33.Location = new System.Drawing.Point(291, 607);
            this.labelInitialize33.Name = "labelInitialize33";
            this.labelInitialize33.Size = new System.Drawing.Size(135, 124);
            this.labelInitialize33.TabIndex = 32;
            this.labelInitialize33.Text = "c";
            this.labelInitialize33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize33.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize32
            // 
            this.labelInitialize32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize32.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize32.Location = new System.Drawing.Point(148, 607);
            this.labelInitialize32.Name = "labelInitialize32";
            this.labelInitialize32.Size = new System.Drawing.Size(135, 124);
            this.labelInitialize32.TabIndex = 31;
            this.labelInitialize32.Text = "c";
            this.labelInitialize32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize32.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize31
            // 
            this.labelInitialize31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize31.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize31.Location = new System.Drawing.Point(5, 607);
            this.labelInitialize31.Name = "labelInitialize31";
            this.labelInitialize31.Size = new System.Drawing.Size(135, 124);
            this.labelInitialize31.TabIndex = 30;
            this.labelInitialize31.Text = "c";
            this.labelInitialize31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize31.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize30
            // 
            this.labelInitialize30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize30.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize30.Location = new System.Drawing.Point(720, 486);
            this.labelInitialize30.Name = "labelInitialize30";
            this.labelInitialize30.Size = new System.Drawing.Size(137, 119);
            this.labelInitialize30.TabIndex = 29;
            this.labelInitialize30.Text = "c";
            this.labelInitialize30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize30.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize29
            // 
            this.labelInitialize29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize29.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize29.Location = new System.Drawing.Point(577, 486);
            this.labelInitialize29.Name = "labelInitialize29";
            this.labelInitialize29.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize29.TabIndex = 28;
            this.labelInitialize29.Text = "c";
            this.labelInitialize29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize29.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize28
            // 
            this.labelInitialize28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize28.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize28.Location = new System.Drawing.Point(434, 486);
            this.labelInitialize28.Name = "labelInitialize28";
            this.labelInitialize28.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize28.TabIndex = 27;
            this.labelInitialize28.Text = "c";
            this.labelInitialize28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize28.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize27
            // 
            this.labelInitialize27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize27.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize27.Location = new System.Drawing.Point(291, 486);
            this.labelInitialize27.Name = "labelInitialize27";
            this.labelInitialize27.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize27.TabIndex = 26;
            this.labelInitialize27.Text = "c";
            this.labelInitialize27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize27.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize26
            // 
            this.labelInitialize26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize26.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize26.Location = new System.Drawing.Point(148, 486);
            this.labelInitialize26.Name = "labelInitialize26";
            this.labelInitialize26.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize26.TabIndex = 25;
            this.labelInitialize26.Text = "c";
            this.labelInitialize26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize26.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize25
            // 
            this.labelInitialize25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize25.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize25.Location = new System.Drawing.Point(5, 486);
            this.labelInitialize25.Name = "labelInitialize25";
            this.labelInitialize25.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize25.TabIndex = 24;
            this.labelInitialize25.Text = "c";
            this.labelInitialize25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize25.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize24
            // 
            this.labelInitialize24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize24.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize24.Location = new System.Drawing.Point(720, 365);
            this.labelInitialize24.Name = "labelInitialize24";
            this.labelInitialize24.Size = new System.Drawing.Size(137, 119);
            this.labelInitialize24.TabIndex = 23;
            this.labelInitialize24.Text = "c";
            this.labelInitialize24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize24.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize23
            // 
            this.labelInitialize23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize23.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize23.Location = new System.Drawing.Point(577, 365);
            this.labelInitialize23.Name = "labelInitialize23";
            this.labelInitialize23.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize23.TabIndex = 22;
            this.labelInitialize23.Text = "c";
            this.labelInitialize23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize23.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize22
            // 
            this.labelInitialize22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize22.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize22.Location = new System.Drawing.Point(434, 365);
            this.labelInitialize22.Name = "labelInitialize22";
            this.labelInitialize22.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize22.TabIndex = 21;
            this.labelInitialize22.Text = "c";
            this.labelInitialize22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize22.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize21
            // 
            this.labelInitialize21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize21.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize21.Location = new System.Drawing.Point(291, 365);
            this.labelInitialize21.Name = "labelInitialize21";
            this.labelInitialize21.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize21.TabIndex = 20;
            this.labelInitialize21.Text = "c";
            this.labelInitialize21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize21.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize20
            // 
            this.labelInitialize20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize20.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize20.Location = new System.Drawing.Point(148, 365);
            this.labelInitialize20.Name = "labelInitialize20";
            this.labelInitialize20.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize20.TabIndex = 19;
            this.labelInitialize20.Text = "c";
            this.labelInitialize20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize20.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize19
            // 
            this.labelInitialize19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize19.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize19.Location = new System.Drawing.Point(5, 365);
            this.labelInitialize19.Name = "labelInitialize19";
            this.labelInitialize19.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize19.TabIndex = 18;
            this.labelInitialize19.Text = "c";
            this.labelInitialize19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize19.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize18
            // 
            this.labelInitialize18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize18.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize18.Location = new System.Drawing.Point(720, 244);
            this.labelInitialize18.Name = "labelInitialize18";
            this.labelInitialize18.Size = new System.Drawing.Size(137, 119);
            this.labelInitialize18.TabIndex = 17;
            this.labelInitialize18.Text = "c";
            this.labelInitialize18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize18.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize17
            // 
            this.labelInitialize17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize17.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize17.Location = new System.Drawing.Point(577, 244);
            this.labelInitialize17.Name = "labelInitialize17";
            this.labelInitialize17.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize17.TabIndex = 16;
            this.labelInitialize17.Text = "c";
            this.labelInitialize17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize17.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize16
            // 
            this.labelInitialize16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize16.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize16.Location = new System.Drawing.Point(434, 244);
            this.labelInitialize16.Name = "labelInitialize16";
            this.labelInitialize16.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize16.TabIndex = 15;
            this.labelInitialize16.Text = "c";
            this.labelInitialize16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize16.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize15
            // 
            this.labelInitialize15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize15.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize15.Location = new System.Drawing.Point(291, 244);
            this.labelInitialize15.Name = "labelInitialize15";
            this.labelInitialize15.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize15.TabIndex = 14;
            this.labelInitialize15.Text = "c";
            this.labelInitialize15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize15.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize14
            // 
            this.labelInitialize14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize14.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize14.Location = new System.Drawing.Point(148, 244);
            this.labelInitialize14.Name = "labelInitialize14";
            this.labelInitialize14.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize14.TabIndex = 13;
            this.labelInitialize14.Text = "c";
            this.labelInitialize14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize14.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize13
            // 
            this.labelInitialize13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize13.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize13.Location = new System.Drawing.Point(5, 244);
            this.labelInitialize13.Name = "labelInitialize13";
            this.labelInitialize13.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize13.TabIndex = 12;
            this.labelInitialize13.Text = "c";
            this.labelInitialize13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize13.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize12
            // 
            this.labelInitialize12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize12.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize12.Location = new System.Drawing.Point(720, 123);
            this.labelInitialize12.Name = "labelInitialize12";
            this.labelInitialize12.Size = new System.Drawing.Size(137, 119);
            this.labelInitialize12.TabIndex = 11;
            this.labelInitialize12.Text = "c";
            this.labelInitialize12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize12.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize11
            // 
            this.labelInitialize11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize11.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize11.Location = new System.Drawing.Point(577, 123);
            this.labelInitialize11.Name = "labelInitialize11";
            this.labelInitialize11.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize11.TabIndex = 10;
            this.labelInitialize11.Text = "c";
            this.labelInitialize11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize11.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize10
            // 
            this.labelInitialize10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize10.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize10.Location = new System.Drawing.Point(434, 123);
            this.labelInitialize10.Name = "labelInitialize10";
            this.labelInitialize10.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize10.TabIndex = 9;
            this.labelInitialize10.Text = "c";
            this.labelInitialize10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize10.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize9
            // 
            this.labelInitialize9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize9.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize9.Location = new System.Drawing.Point(291, 123);
            this.labelInitialize9.Name = "labelInitialize9";
            this.labelInitialize9.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize9.TabIndex = 8;
            this.labelInitialize9.Text = "c";
            this.labelInitialize9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize9.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize8
            // 
            this.labelInitialize8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize8.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize8.Location = new System.Drawing.Point(148, 123);
            this.labelInitialize8.Name = "labelInitialize8";
            this.labelInitialize8.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize8.TabIndex = 7;
            this.labelInitialize8.Text = "c";
            this.labelInitialize8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize8.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize7
            // 
            this.labelInitialize7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize7.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize7.Location = new System.Drawing.Point(5, 123);
            this.labelInitialize7.Name = "labelInitialize7";
            this.labelInitialize7.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize7.TabIndex = 6;
            this.labelInitialize7.Text = "c";
            this.labelInitialize7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize7.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize6
            // 
            this.labelInitialize6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize6.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize6.Location = new System.Drawing.Point(720, 2);
            this.labelInitialize6.Name = "labelInitialize6";
            this.labelInitialize6.Size = new System.Drawing.Size(137, 119);
            this.labelInitialize6.TabIndex = 5;
            this.labelInitialize6.Text = "c";
            this.labelInitialize6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize6.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize5
            // 
            this.labelInitialize5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize5.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize5.Location = new System.Drawing.Point(577, 2);
            this.labelInitialize5.Name = "labelInitialize5";
            this.labelInitialize5.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize5.TabIndex = 4;
            this.labelInitialize5.Text = "c";
            this.labelInitialize5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize5.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize4
            // 
            this.labelInitialize4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize4.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize4.Location = new System.Drawing.Point(434, 2);
            this.labelInitialize4.Name = "labelInitialize4";
            this.labelInitialize4.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize4.TabIndex = 3;
            this.labelInitialize4.Text = "c";
            this.labelInitialize4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize4.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize3
            // 
            this.labelInitialize3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize3.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize3.Location = new System.Drawing.Point(291, 2);
            this.labelInitialize3.Name = "labelInitialize3";
            this.labelInitialize3.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize3.TabIndex = 2;
            this.labelInitialize3.Text = "c";
            this.labelInitialize3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize3.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize2
            // 
            this.labelInitialize2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize2.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize2.Location = new System.Drawing.Point(148, 2);
            this.labelInitialize2.Name = "labelInitialize2";
            this.labelInitialize2.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize2.TabIndex = 1;
            this.labelInitialize2.Text = "c";
            this.labelInitialize2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize2.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize1
            // 
            this.labelInitialize1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize1.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize1.Location = new System.Drawing.Point(5, 2);
            this.labelInitialize1.Name = "labelInitialize1";
            this.labelInitialize1.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize1.TabIndex = 0;
            this.labelInitialize1.Text = "c";
            this.labelInitialize1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize1.Click += new System.EventHandler(this.memoClick);
            // 
            // timerMemory
            // 
            this.timerMemory.Interval = 800;
            this.timerMemory.Tick += new System.EventHandler(this.timerMemory_Tick);
            // 
            // timerResultTime
            // 
            this.timerResultTime.Interval = 1;
            this.timerResultTime.Tick += new System.EventHandler(this.timerResultTime_Tick);
            // 
            // timerAction
            // 
            this.timerAction.Interval = 1;
            this.timerAction.Tick += new System.EventHandler(this.timerAction_Tick);
            // 
            // FormGameBig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(862, 733);
            this.Controls.Add(this.tableLayoutPanelGame);
            this.Name = "FormGameBig";
            this.Text = "Gra Memory";
            this.tableLayoutPanelGame.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGame;
        private System.Windows.Forms.Label labelInitialize36;
        private System.Windows.Forms.Label labelInitialize35;
        private System.Windows.Forms.Label labelInitialize34;
        private System.Windows.Forms.Label labelInitialize33;
        private System.Windows.Forms.Label labelInitialize32;
        private System.Windows.Forms.Label labelInitialize31;
        private System.Windows.Forms.Label labelInitialize30;
        private System.Windows.Forms.Label labelInitialize29;
        private System.Windows.Forms.Label labelInitialize28;
        private System.Windows.Forms.Label labelInitialize27;
        private System.Windows.Forms.Label labelInitialize26;
        private System.Windows.Forms.Label labelInitialize25;
        private System.Windows.Forms.Label labelInitialize24;
        private System.Windows.Forms.Label labelInitialize23;
        private System.Windows.Forms.Label labelInitialize22;
        private System.Windows.Forms.Label labelInitialize21;
        private System.Windows.Forms.Label labelInitialize20;
        private System.Windows.Forms.Label labelInitialize19;
        private System.Windows.Forms.Label labelInitialize18;
        private System.Windows.Forms.Label labelInitialize17;
        private System.Windows.Forms.Label labelInitialize16;
        private System.Windows.Forms.Label labelInitialize15;
        private System.Windows.Forms.Label labelInitialize14;
        private System.Windows.Forms.Label labelInitialize13;
        private System.Windows.Forms.Label labelInitialize12;
        private System.Windows.Forms.Label labelInitialize11;
        private System.Windows.Forms.Label labelInitialize10;
        private System.Windows.Forms.Label labelInitialize9;
        private System.Windows.Forms.Label labelInitialize8;
        private System.Windows.Forms.Label labelInitialize7;
        private System.Windows.Forms.Label labelInitialize6;
        private System.Windows.Forms.Label labelInitialize5;
        private System.Windows.Forms.Label labelInitialize4;
        private System.Windows.Forms.Label labelInitialize3;
        private System.Windows.Forms.Label labelInitialize2;
        private System.Windows.Forms.Label labelInitialize1;
        private System.Windows.Forms.Timer timerMemory;
        private System.Windows.Forms.Timer timerResultTime;
        private System.Windows.Forms.Timer timerAction;
    }
}