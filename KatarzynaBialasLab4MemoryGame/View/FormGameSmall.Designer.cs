﻿namespace KatarzynaBialasLab4MemoryGame
{
    partial class FormGameSmall
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanelGame = new System.Windows.Forms.TableLayoutPanel();
            this.labelInitialize1 = new System.Windows.Forms.Label();
            this.labelInitialize2 = new System.Windows.Forms.Label();
            this.labelInitialize3 = new System.Windows.Forms.Label();
            this.labelInitialize4 = new System.Windows.Forms.Label();
            this.labelInitialize5 = new System.Windows.Forms.Label();
            this.labelInitialize6 = new System.Windows.Forms.Label();
            this.labelInitialize7 = new System.Windows.Forms.Label();
            this.labelInitialize8 = new System.Windows.Forms.Label();
            this.labelInitialize9 = new System.Windows.Forms.Label();
            this.labelInitialize10 = new System.Windows.Forms.Label();
            this.labelInitialize11 = new System.Windows.Forms.Label();
            this.labelInitialize12 = new System.Windows.Forms.Label();
            this.labelInitialize13 = new System.Windows.Forms.Label();
            this.labelInitialize14 = new System.Windows.Forms.Label();
            this.labelInitialize15 = new System.Windows.Forms.Label();
            this.labelInitialize16 = new System.Windows.Forms.Label();
            this.timerMemory = new System.Windows.Forms.Timer(this.components);
            this.timerResultTime = new System.Windows.Forms.Timer(this.components);
            this.timerAction = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanelGame.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelGame
            // 
            this.tableLayoutPanelGame.BackColor = System.Drawing.Color.CornflowerBlue;
            this.tableLayoutPanelGame.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanelGame.ColumnCount = 4;
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelGame.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize16, 3, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize15, 2, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize14, 1, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize13, 0, 3);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize12, 3, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize11, 2, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize10, 1, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize9, 0, 2);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize8, 3, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize7, 2, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize6, 1, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize5, 0, 1);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize4, 3, 0);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize3, 2, 0);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize2, 1, 0);
            this.tableLayoutPanelGame.Controls.Add(this.labelInitialize1, 0, 0);
            this.tableLayoutPanelGame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelGame.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelGame.Name = "tableLayoutPanelGame";
            this.tableLayoutPanelGame.RowCount = 4;
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelGame.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanelGame.Size = new System.Drawing.Size(571, 488);
            this.tableLayoutPanelGame.TabIndex = 0;
            // 
            // labelInitialize1
            // 
            this.labelInitialize1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize1.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize1.Location = new System.Drawing.Point(5, 2);
            this.labelInitialize1.Name = "labelInitialize1";
            this.labelInitialize1.Size = new System.Drawing.Size(134, 119);
            this.labelInitialize1.TabIndex = 0;
            this.labelInitialize1.Text = "s";
            this.labelInitialize1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize1.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize2
            // 
            this.labelInitialize2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize2.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize2.Location = new System.Drawing.Point(147, 2);
            this.labelInitialize2.Name = "labelInitialize2";
            this.labelInitialize2.Size = new System.Drawing.Size(134, 119);
            this.labelInitialize2.TabIndex = 1;
            this.labelInitialize2.Text = "s";
            this.labelInitialize2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize2.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize3
            // 
            this.labelInitialize3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize3.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize3.Location = new System.Drawing.Point(289, 2);
            this.labelInitialize3.Name = "labelInitialize3";
            this.labelInitialize3.Size = new System.Drawing.Size(134, 119);
            this.labelInitialize3.TabIndex = 2;
            this.labelInitialize3.Text = "s";
            this.labelInitialize3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize3.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize4
            // 
            this.labelInitialize4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize4.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize4.Location = new System.Drawing.Point(431, 2);
            this.labelInitialize4.Name = "labelInitialize4";
            this.labelInitialize4.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize4.TabIndex = 3;
            this.labelInitialize4.Text = "s";
            this.labelInitialize4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize4.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize5
            // 
            this.labelInitialize5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize5.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize5.Location = new System.Drawing.Point(5, 123);
            this.labelInitialize5.Name = "labelInitialize5";
            this.labelInitialize5.Size = new System.Drawing.Size(134, 119);
            this.labelInitialize5.TabIndex = 4;
            this.labelInitialize5.Text = "s";
            this.labelInitialize5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize5.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize6
            // 
            this.labelInitialize6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize6.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize6.Location = new System.Drawing.Point(147, 123);
            this.labelInitialize6.Name = "labelInitialize6";
            this.labelInitialize6.Size = new System.Drawing.Size(134, 119);
            this.labelInitialize6.TabIndex = 5;
            this.labelInitialize6.Text = "s";
            this.labelInitialize6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize6.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize7
            // 
            this.labelInitialize7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize7.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize7.Location = new System.Drawing.Point(289, 123);
            this.labelInitialize7.Name = "labelInitialize7";
            this.labelInitialize7.Size = new System.Drawing.Size(134, 119);
            this.labelInitialize7.TabIndex = 6;
            this.labelInitialize7.Text = "s";
            this.labelInitialize7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize7.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize8
            // 
            this.labelInitialize8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize8.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize8.Location = new System.Drawing.Point(431, 123);
            this.labelInitialize8.Name = "labelInitialize8";
            this.labelInitialize8.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize8.TabIndex = 7;
            this.labelInitialize8.Text = "s";
            this.labelInitialize8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize8.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize9
            // 
            this.labelInitialize9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize9.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize9.Location = new System.Drawing.Point(5, 244);
            this.labelInitialize9.Name = "labelInitialize9";
            this.labelInitialize9.Size = new System.Drawing.Size(134, 119);
            this.labelInitialize9.TabIndex = 8;
            this.labelInitialize9.Text = "s";
            this.labelInitialize9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize9.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize10
            // 
            this.labelInitialize10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize10.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize10.Location = new System.Drawing.Point(147, 244);
            this.labelInitialize10.Name = "labelInitialize10";
            this.labelInitialize10.Size = new System.Drawing.Size(134, 119);
            this.labelInitialize10.TabIndex = 9;
            this.labelInitialize10.Text = "s";
            this.labelInitialize10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize10.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize11
            // 
            this.labelInitialize11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize11.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize11.Location = new System.Drawing.Point(289, 244);
            this.labelInitialize11.Name = "labelInitialize11";
            this.labelInitialize11.Size = new System.Drawing.Size(134, 119);
            this.labelInitialize11.TabIndex = 10;
            this.labelInitialize11.Text = "s";
            this.labelInitialize11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize11.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize12
            // 
            this.labelInitialize12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize12.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize12.Location = new System.Drawing.Point(431, 244);
            this.labelInitialize12.Name = "labelInitialize12";
            this.labelInitialize12.Size = new System.Drawing.Size(135, 119);
            this.labelInitialize12.TabIndex = 11;
            this.labelInitialize12.Text = "s";
            this.labelInitialize12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize12.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize13
            // 
            this.labelInitialize13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize13.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize13.Location = new System.Drawing.Point(5, 365);
            this.labelInitialize13.Name = "labelInitialize13";
            this.labelInitialize13.Size = new System.Drawing.Size(134, 121);
            this.labelInitialize13.TabIndex = 12;
            this.labelInitialize13.Text = "s";
            this.labelInitialize13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize13.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize14
            // 
            this.labelInitialize14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize14.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize14.Location = new System.Drawing.Point(147, 365);
            this.labelInitialize14.Name = "labelInitialize14";
            this.labelInitialize14.Size = new System.Drawing.Size(134, 121);
            this.labelInitialize14.TabIndex = 13;
            this.labelInitialize14.Text = "s";
            this.labelInitialize14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize14.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize15
            // 
            this.labelInitialize15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize15.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize15.Location = new System.Drawing.Point(289, 365);
            this.labelInitialize15.Name = "labelInitialize15";
            this.labelInitialize15.Size = new System.Drawing.Size(134, 121);
            this.labelInitialize15.TabIndex = 14;
            this.labelInitialize15.Text = "s";
            this.labelInitialize15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize15.Click += new System.EventHandler(this.memoClick);
            // 
            // labelInitialize16
            // 
            this.labelInitialize16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInitialize16.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelInitialize16.Location = new System.Drawing.Point(431, 365);
            this.labelInitialize16.Name = "labelInitialize16";
            this.labelInitialize16.Size = new System.Drawing.Size(135, 121);
            this.labelInitialize16.TabIndex = 15;
            this.labelInitialize16.Text = "s";
            this.labelInitialize16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelInitialize16.Click += new System.EventHandler(this.memoClick);
            // 
            // timerMemory
            // 
            this.timerMemory.Interval = 800;
            this.timerMemory.Tick += new System.EventHandler(this.timerMemory_Tick);
            // 
            // timerResultTime
            // 
            this.timerResultTime.Interval = 1;
            this.timerResultTime.Tick += new System.EventHandler(this.timerResultTime_Tick);
            // 
            // timerAction
            // 
            this.timerAction.Interval = 1;
            this.timerAction.Tick += new System.EventHandler(this.timerAction_Tick);
            // 
            // FormGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 488);
            this.Controls.Add(this.tableLayoutPanelGame);
            this.Name = "FormGame";
            this.Text = "Gra Memory";
            this.tableLayoutPanelGame.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelGame;
        private System.Windows.Forms.Label labelInitialize16;
        private System.Windows.Forms.Label labelInitialize15;
        private System.Windows.Forms.Label labelInitialize14;
        private System.Windows.Forms.Label labelInitialize13;
        private System.Windows.Forms.Label labelInitialize12;
        private System.Windows.Forms.Label labelInitialize11;
        private System.Windows.Forms.Label labelInitialize10;
        private System.Windows.Forms.Label labelInitialize9;
        private System.Windows.Forms.Label labelInitialize8;
        private System.Windows.Forms.Label labelInitialize7;
        private System.Windows.Forms.Label labelInitialize6;
        private System.Windows.Forms.Label labelInitialize5;
        private System.Windows.Forms.Label labelInitialize4;
        private System.Windows.Forms.Label labelInitialize3;
        private System.Windows.Forms.Label labelInitialize2;
        private System.Windows.Forms.Label labelInitialize1;
        private System.Windows.Forms.Timer timerMemory;
        private System.Windows.Forms.Timer timerResultTime;
        private System.Windows.Forms.Timer timerAction;
    }
}